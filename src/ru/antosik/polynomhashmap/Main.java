package ru.antosik.polynomhashmap;

import ru.antosik.polynomhashmap.Classes.Polynom;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        Polynom poly1 = new Polynom(new HashMap<>() {{
            put(0, -2);
            put(1, 2);
            put(2, 8);
        }});
        Polynom poly2 = new Polynom(new HashMap<>() {{
            put(0, 1);
            put(1, 4);
            put(2, 8);
            put(3, 8);
        }});

        try {
            Polynom sum = Polynom.sum(poly1, poly2);
            System.out.println(sum);
        } catch (Exception e) {
            System.err.println(String.format("Error! %s", e.getMessage()));
        }
    }
}

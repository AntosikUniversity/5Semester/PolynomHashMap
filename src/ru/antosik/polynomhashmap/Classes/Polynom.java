package ru.antosik.polynomhashmap.Classes;

import java.util.HashMap;

public class Polynom {
    private final HashMap<Integer, Integer> degreeRatioMap;

    public Polynom(HashMap<Integer, Integer> degreeRatioMap) {
        this.degreeRatioMap = degreeRatioMap;
    }

    public static Polynom sum(Polynom first, Polynom second) {
        if ((first == null || first.isNull()) && (second == null || second.isNull()))
            return new Polynom(new HashMap<>());
        if (first == null || first.isNull())
            return second;
        if (second == null || second.isNull())
            return first;
        if (
                first.getDegreeRatioMap().keySet().stream().max((f, s) -> f < s ? s : f) !=
                        second.getDegreeRatioMap().keySet().stream().max((f, s) -> f < s ? s : f)
                ) throw new IllegalArgumentException("Degree must be equal!");

        HashMap<Integer, Integer> result = new HashMap<>(first.getDegreeRatioMap());

        second.getDegreeRatioMap().forEach((key, value) -> result.merge(key, value, (coef1, coef2) -> coef1 + coef2));

        return new Polynom(result);
    }

    public HashMap<Integer, Integer> getDegreeRatioMap() {
        return degreeRatioMap;
    }

    public boolean isNull() {
        return degreeRatioMap.isEmpty() || degreeRatioMap.values().stream().allMatch(integer -> integer == 0);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        degreeRatioMap.forEach((key, value) -> sb.append(String.format("+%d*x^%d", value, key)));
        return sb.toString();
    }
}
